var gulp        = require('gulp');
var compass     = require('gulp-compass');
var gutil       = require('gulp-util');
var uglify      = require('gulp-uglify');
var livereload  = require('gulp-livereload');


// Compilar JS
gulp.task('scripts', function(){
    gulp.src('resources/assets/js/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('public/assets/web/js'))
        .pipe(livereload());
});

// Compilar SCSS.
gulp.task('compass', function() {
    return gulp.src('resources/assets/scss/*.scss')
        .pipe(compass({
            config_file: 'resources/assets/config.rb',
            sass: 'resources/assets/scss',
            image: 'resources/assets/images'
        })
            .on('error', gutil.log))
        .pipe(gulp.dest('public/assets/web/css'))
        .pipe(livereload());
});

gulp.task('watch', function(){
    livereload.listen()
    gulp.watch('resources/assets/scss/partials/*.scss').on('change', livereload.changed);
    gulp.watch('resources/assets/scss/*.scss', ['compass']);
    gulp.watch('resources/assets/js/*.js', ['scripts']);
});

gulp.task('default', ['scripts', 'compass', 'watch']);
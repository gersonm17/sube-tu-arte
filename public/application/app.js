'user strict';

//var adminApp = angular.module('adminApp', ['ngRoute', 'ngResource', 'angular-loading-bar', 'ui.bootstrap', 'ui.bootstrap.progressbar', 'ui.tree', 'checklist-model','ui.mask']);
var geekAdmin = angular.module('geekAdmin', ['ngAnimate', 'ngRoute', 'ngResource', 'foundation']);

geekAdmin.config(function($routeProvider){
	$routeProvider
		.when('/', { templateUrl:appConfig.views + '/dashboard'})
		.when('/users/:segmentID?', { templateUrl:appConfig.views + '/users'})
		.when('/security',{ templateUrl:appConfig.views + '/security'})
		.when('/config',{ templateUrl:appConfig.views + '/config'})
		.otherwise({ redirectTo: '/' });

});

geekAdmin.controller('HomeController', function ($scope) {
	$scope.users = [
		{'name': 'Admin', 'mail': 'gerson@geekadvice.pe'},
		{'name': 'Gerson', 'mail': 'gerson@geekadvice.pe'},
		{'name': 'Roberto Gonzales', 'mail': 'gerson@geekadvice.pe'},
		{'name': 'Pedro Martinez', 'mail': 'gerson@geekadvice.pe'},
		{'name': 'Otro nombre', 'mail': 'gerson@geekadvice.pe'}
	];
});
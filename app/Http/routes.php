<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');
Route::post('buscar', 'HomeController@buscar');
Route::get('home', 'HomeController@home');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

// Paginas estaticas.
Route::get('terminos', [
    'as'   => 'pag.terms',
    'uses' => 'Page\PageController@termsPage'
]);

Route::get('privacidad', [
    'as'   => 'pag.privacy',
    'uses' => 'Page\PageController@privacyPage'
]);

Route::get('politicas', [
    'as'   => 'pag.policies',
    'uses' => 'Page\PageController@policiesPage'
]);

Route::get('sugerencias', [
    'as'   => 'pag.suggestions',
    'uses' => 'Page\PageController@suggestionsPage'
]);

Route::get('nosotros', [
    'as'   => 'pag.about',
    'uses' => 'Page\PageController@aboutPage'
]);

// Admin
Route::group(array('prefix'=>'admin', 'middleware'=>'auth'), function(){
	Route::controller('/', 'Admin\HomeController');
});

Route::group(array('prefix'=>'views', 'middleware'=>'auth'), function(){
	Route::get('dashboard', function(){ return view('admin.dashboard'); });
	Route::get('users', function(){ return view('admin.users'); });
	Route::get('security', function(){ return view('admin.security'); });
	Route::get('config', function(){ return view('admin.config'); });
});

// API
Route::group(array('prefix'=>'api/v1', 'middleware'=>'auth.basic'), function(){
	Route::controller('users', 'ApiV1\ApiUsersController');
	Route::get('/', function(){ return 'Geek Advice API V1'; });
});

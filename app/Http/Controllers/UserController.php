<?php namespace App\Http\Controllers;

use App\Commands\ApiResponse;
use App\Commands\UserCommand;
use Illuminate\Support\Facades\Input;
use App\Models\User;
use Auth;
use Hash;
use Illuminate\Support\Facades\Response;

class UserController extends Controller
{
	public function anyNewuser()
	{
		return UserCommand::newuser('prueba','jflorez@geek332advice.pe','1234233356','1','1');
	}

	public function anyListuser()
	{
		$r = new ApiResponse();
		$user=User::get()->load('roles');
		$r->data=$user;
		return Response::json($r);
	}

	public function postLogin()
	{
		try
		{
			$password = bcrypt('password');
			return $password();

			$auth=Auth::attempt(['email' => Input::get('email'), 'password' => Input::get('password')]);


			return var_dump($auth);
			if(Auth::user()->is('Administrador'))
				return Redirect::to('/admin');
			else
				return Redirect::to('/');
		}catch (Exception $e)
		{
			$msg = 'Error desconocido';
			switch(get_class($e))
			{
				case 'Toddish\Verify\UserNotFoundException':
					$msg = 'No podemos encontrar este nombre de usuario';
					break;
				case 'Toddish\Verify\UserUnverifiedException':
					$msg = 'Tu usuario no se encuentra verificado, por favor revisa tu correo o contacta a un administrador';
					break;
				case 'Toddish\Verify\UserDisabledException':
					$msg = 'Tu usuario se encuentra deshabilitado';
					break;
				case 'Toddish\Verify\UserDeletedException':
					$msg = 'Este usuario a sido eliminado';
					break;
				case 'Toddish\Verify\UserPasswordIncorrectException':
					$msg = 'Por favor verifica tu contraseña.';
					break;
			}

			return Redirect::to('login')->withErrors($msg);
		}
	}
}
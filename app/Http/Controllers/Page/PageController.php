<?php namespace App\Http\Controllers\Page;

use App\Http\Controllers\Controller;

class PageController extends Controller
{

    public function termsPage()
    {
        return view('page.terms');
    }

    public function privacyPage(){
        return view('page.privacy');
    }

    public function policiesPage(){
        return view('page.policies');
    }

    public function suggestionsPage(){
        return view('page.suggestions');
    }

    public function aboutPage(){
        return view('page.about');
    }
}
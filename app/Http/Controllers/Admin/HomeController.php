<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class HomeController extends Controller {

	public function __construct()
	{
		//$this->middleware('guest');
	}

	public function anyIndex()
	{
		return view('admin.home');
	}

	public function anyStyle()
	{
		return view('ci.styles');
	}
}

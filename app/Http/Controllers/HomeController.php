<?php namespace App\Http\Controllers;

class HomeController extends Controller {

	public function __construct()
	{
		//$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('home.index');
	}

	public function home()
	{
		return view('home');
	}

	public function buscar()
	{
		return "hola";
	}

}

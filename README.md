# Geek Advice - Laravel projects

Proyecto Sueb Tu Arte

## Versiones

Laravel 5.0

## Change Log

v0.01

- Instalación limpia de laravel 5.0.14
- Creación de .env con variables de base de datos, timezone y debug
- Campos adicionales para users agregados
- Creación de carpeta con idioma español
- Instalación e integración de gulp con elixir
- Instalación de bower
- Versiones separadas en frontend para website y applications usando bower y gulp
- Migraciones para usuarios, roles, y permisos.

##TODO

- Crear sistema para gestionar roles y permisos        //Justo y Yuri
- Gestionar sesiones y listar usuarios logueados       //Justo
- Crear rutas y vistas para login                      //Justo Yuri
- Pruebas unitarias para roles y permisos              //Yuri
- Separar versiones para usuarios compatibles con IE8  //JC
    - Validaciones con jquery                             //JC
    - Input Mask                                          //JC
    - Testing de velocidad                                //JC
- Versión para administrador con versión mínima de IE9 //Carlos
    - Evaluar validaciones solo con Angular               //Carlos
    - Tratar de no usar jquery                            //Carlos
    - Probar minify para angular y testear css            //Carlos y Gerson
- Considerar modulo CMS                                //Gerson
- Manejo de idiomas                                    //Gerson
- Crear diagrama de roles persmisos usuarios
- Crear carpeta admin en public para administrador
- CMS por bloques y vistas, integración con onfirmación y definir documentación.

## Instalación

- Instalar NodeJS
- Instalar gulp npm install -g gulp
- Instalar bower npm install -g bower
- Instalar elixir npm install
- Ejecutar en consola bower init //crea el bower.json
- Crear archivo de configuración para bower.
- Para compilar solo js usar gulp scripts
- Para compilar solo css usar gulp styles
- Para producción ejecutar gulp --production


## Notes

- No usar el CSRF, bloquea la vista!!

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

### License

Es privado!
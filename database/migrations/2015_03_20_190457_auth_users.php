<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AuthUsers extends Migration {

	private $prefix = '';
	
	public function up()
	{
		
		// Create the permissions table
		Schema::create($this->prefix.'permissions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 100)->index();
			$table->string('description', 255)->nullable();
			$table->timestamps();
		});

		// Create the roles table
		Schema::create($this->prefix.'roles', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 100)->index();
			$table->string('description', 255)->nullable();
			$table->integer('level');
			$table->timestamps();
		});

		// Create the users table
		Schema::create($this->prefix.'users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('password', 60);
			$table->string('name', 60);
			$table->string('last_name', 60)->nullable();
			$table->string('email', 255)->unique()->index();
			$table->boolean('confirmed')->default(0);
			$table->boolean('disabled')->default(0);
			$table->rememberToken();
			$table->string('token')->nullable();
			$table->integer('last_activity')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});

		// Create the role/user relationship table
		Schema::create($this->prefix.'role_user', function(Blueprint $table)
		{
			$table->integer('user_id')->unsigned()->index();
			$table->integer('role_id')->unsigned()->index();
			$table->timestamps();

			$table->foreign('user_id')->references('id')->on($this->prefix.'users')->onDelete('cascade');
			$table->foreign('role_id')->references('id')->on($this->prefix.'roles')->onDelete('cascade');
		});

		// Create the permission/role relationship table
		Schema::create($this->prefix.'permission_role', function(Blueprint $table)
		{
			$table->integer('permission_id')->unsigned()->index();
			$table->integer('role_id')->unsigned()->index();
			$table->timestamps();

			$table->foreign('permission_id')->references('id')->on($this->prefix.'permissions')->onDelete('cascade');
			$table->foreign('role_id')->references('id')->on($this->prefix.'roles')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop($this->prefix.'role_user');
		Schema::drop($this->prefix.'permission_role');
		Schema::drop($this->prefix.'users');
		Schema::drop($this->prefix.'roles');
		Schema::drop($this->prefix.'permissions');
	}

}

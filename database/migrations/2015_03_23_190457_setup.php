<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Setup extends Migration {

	public function up()
	{
		
		// Create the permissions table
		Schema::create('profiles', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned()->index();
			$table->string('is_public')->default('1');
			$table->integer('visits');
			$table->string('country');
			$table->string('province');
			$table->string('district');
			$table->timestamps();
		});

		Schema::create('portfolios', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned()->index();
			$table->string('title');
			$table->string('description');
			$table->integer('score');
			$table->integer('type');
			$table->timestamps();
		});

		Schema::create('qualities', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('profile_id')->unsigned()->index();
			$table->string('description');
			$table->string('is_editable');
			$table->string('is_visible');
			$table->timestamps();
		});

		Schema::create('reports', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned()->index();
			$table->integer('reported_id')->unsigned()->index();
			$table->string('reason');
			$table->timestamps();
		});

		Schema::create('works', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('portfolio_id')->unsigned()->index();
			$table->string('title');
			$table->string('description');
			$table->string('file_path');
			$table->timestamps();
		});

		Schema::create('comments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned()->index();
			$table->integer('work_id')->unsigned()->index();
			$table->string('comment');
			$table->string('parent_id');
			$table->timestamps();
		});

		Schema::create('contest', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('category_id')->unsigned()->index();
			$table->timestamps();
		});

		Schema::create('votes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned()->index();
			$table->integer('work_id')->unsigned()->index();
			$table->timestamps();
		});

		Schema::create('category', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('category_id')->unsigned()->index();
			$table->timestamps();
		});

		Schema::create('relationships', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('friend_a')->unsigned()->index();
			$table->integer('friend_b')->unsigned()->index();
			$table->integer('type')->unsigned()->index();
			$table->integer('confirmed')->unsigned()->index();
			$table->timestamps();
		});

		Schema::create('contest_work', function(Blueprint $table)
		{
			$table->integer('work_id')->unsigned()->index();
			$table->integer('contest_id')->unsigned()->index();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop($this->prefix.'role_user');
	}

}

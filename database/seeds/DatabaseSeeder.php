<?php

use Illuminate\Database\Seeder;

use App\Models\User;
use App\Models\Role;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		Role::create(['name'=>'Admin','description'=>'Administrador de sistema', 'level'=>1]);
		Role::create(['name'=>'Owner', 'description'=>'Administrador de sistema', 'level'=>2]);
		Role::create(['name'=>'Customer', 'description'=>'Usuario de aplicación solo puede modificar sus propios registros', 'level'=>3]);

		$user =User::create([
			'name' => 'Gerson',
			'last_name' => 'Aduviri',
			'email' => 'gerson@geekadvice.pe',
			'password' => '123456',
			'confirmed' => '1',
			'disabled' => '0'
		]);
		$user->save();
		$user->roles()->attach(1);
	}

}

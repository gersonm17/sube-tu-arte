@include('layout/head')

<div class="main_content clearfix">
	<aside class="sidebar">
		@include('layout/partials/nav')
	</aside>

	<section class="content">
		@yield('content')	
	</section>
</div>

@include('layout/footer')

<!Doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Sube Tu Arte</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">

    {!! HTML::style('assets/web/css/normalize.min.css') !!}
    {!! HTML::style('assets/web/css/style.css') !!}

    {!! HTML::script('assets/web/js/vendor/modernizr-2.8.3.min.js') !!}

</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
    <div class="container">
        <header id="header">
            <section class="logo">
                <a href="{{ url('/') }}" title="Sube tu arte">
                    <img src="{{ asset('assets/web/images/sube-tu-arte.png') }}" alt="Sube tu arte">
                </a>
            </section>

            <section class="menu_acciones">
                <button id="mostrar_menu_principal" class="button_accion"><span class="icon-menu-principal"></span></button>

                @include('layout/partials/form_search')

                <button id="mostrar_menu_personal" class="button_accion"><span class="icon-menu-personal"></span></button>

                <nav class="nav_persona">
                    <ul class="menu_personal">
                        <li><a href="#" title=""><span class="icon-ver"></span></a></li>
                        <li><a href="#" title=""><span class="icon-mizona"></span></a></li>
                        <li><a href="#" title=""><span class="icon-amigos"></span></a></li>
                        <li><a href="#" title=""><span class="icon-upload"></span></a></li>
                        <li class="icono_perfil"><a href="#" title="">{!! HTML::image('assets/web/images/perfil.jpg', 'Mi perfil sube tu arte') !!}</a></li>
                    </ul>
                </nav>
            </section>
        </header>
    </div>
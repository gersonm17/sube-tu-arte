<footer>
	<div class="container">
		<figure class="logo_footer">
			{!! HTML::image('assets/web/images/sube-tu-arte.png', 'Sube tu arte') !!}
		</figure>

		<ul class="location">
			<li>Idioma: <strong>Espaniol</strong></li>
			<li>Pais: <strong>Peru</strong></li>
		</ul>

		<section class="bottom">
			<section class="item_bottom">
				<h3>Informacion</h3>
				<ul class="info_footer">
					<li><a href="{{ route('pag.terms') }}" title="">Terminos</a></li>
				</ul>
			</section>

			<section class="item_bottom">
				<h3>Prensa</h3>
				<ul class="info_footer">
					<li><a href="{{ route('pag.privacy') }}" title="">Privacidad</a></li>
				</ul>
			</section>

			<section class="item_bottom">
				<h3>Derechos de CopyRight</h3>
				<ul class="info_footer">
					<li><a href="{{ route('pag.policies') }}" title="">Politicas y privacidad</a></li>
				</ul>
			</section>

			<section class="item_bottom">
				<h3>Creadores</h3>
				<ul class="info_footer">
					<li><a href="{{ route('pag.suggestions') }}" title="">sugerencias</a></li>
				</ul>
			</section>

			<section class="item_bottom">
				<h3>Fundadores</h3>
				<ul class="info_footer">
					<li><a href="{{ route('pag.about') }}" title="">+Subetuarte</a></li>
				</ul>
			</section>
		</section>

		<section class="section_menu_footer">
			<ul class="menu_footer">
				<li><a href="#" title="">Musicales</a></li>
				<li><a href="#" title="">Visuales</a></li>
				<li><a href="#" title="">Teatrales</a></li>
				<li><a href="#" title="">Escritas</a></li>
				<li><a href="#" title="">Ver todo</a></li>
				<li><a href="#" title="">Mi zona</a></li>
				<li><a href="#" title="">Amigos</a></li>
				<li><a href="#" title="">SubeTuArte</a></li>
			</ul>
		</section>
	</div>
</footer>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="{{ asset('assets/web/js/vendor/jquery-1.11.2.min.js') }}"><\/script>')</script>

{!! HTML::script('assets/web/js/plugins.js') !!}
{!! HTML::script('assets/web/js/main.js') !!}
{!! HTML::script('assets/web/js/app.js') !!}

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-XXXXX-X','auto');ga('send','pageview');
</script>
</body>
</html>
{!! Form::open(['url' => 'buscar', 'method' => 'POST', 'id' => 'form_search', 'files' => 'true']) !!}
	<div class="form_group">
		{!! Form::text('buscar', null, array('placeholder' => 'Buscar...', 'class' => 'form_control' )) !!}
	</div>	
{!! Form::close() !!}
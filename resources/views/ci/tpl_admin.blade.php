<!DOCTYPE html>
<html lang="es" ng-app="geekAdmin">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>Sube Tu Arte :. {{ $title or 'Administración' }}</title>
	<!--<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300|Roboto+Condensed:400,300' rel='stylesheet' type='text/css' />-->
	<link href="{{ asset('css/style.css') }}" rel="stylesheet" />
	<link href="{{ asset('output/all_app.css') }}" rel="stylesheet" />

	<!--[if lt IE 9]>
	<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
	<script src="//s3.amazonaws.com/nwapi/nwmatcher/nwmatcher-1.2.5-min.js"></script>
	<script src="//html5base.googlecode.com/svn-history/r38/trunk/js/selectivizr-1.0.3b.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.1.0/respond.min.js"></script>
	<![endif]-->

	@yield('header')
</head>
<body>
@yield('content')
@yield('footer')
<!-- Scripts -->
<script type="text/javascript">
	var appConfig = {};
	appConfig.root = '{{ URL::to('/') }}';
	appConfig.apiURL = '{{ URL::to('api/v1') }}';
	appConfig.views = '{{ URL::to('/views') }}';
</script>
<script src="{{ asset('output/all_app.js') }}"></script>
@yield('scripts')
</body>
</html>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Sube Tu Arte :. {{ $title or 'Bienvenido' }}</title>
	<link href='//fonts.googleapis.com/css?family=Roboto-Condensed:400,300' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300|Roboto+Condensed:400,300' rel='stylesheet' type='text/css'>
	<link href="{{ asset('output/all.css') }}" rel="stylesheet">

	<!--[if lt IE 9]>
	<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
	<script src="//s3.amazonaws.com/nwapi/nwmatcher/nwmatcher-1.2.5-min.js"></script>
	<script src="//html5base.googlecode.com/svn-history/r38/trunk/js/selectivizr-1.0.3b.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.1.0/respond.min.js"></script>
	<script src="{{ asset('js/rem.js') }}" type="text/javascript"></script>
	<![endif]-->

	@yield('header')
</head>
<body>
<div id="header">
	<div class="right">
		@if (!Auth::guest())
		<a href="{{ url('/admin') }}">Administrador</a> | <a href="{{ url('/auth/logout') }}">Cerrar sessión</a>
		@else
		<a href="{{ url('/auth/login') }}">Iniciar sessión</a>
		@endif
	</div>
</div>

@yield('content')
@yield('footer')

<!-- Scripts -->
<script src="{{ asset('output/all.js') }}"></script>
@yield('scripts')
</body>
</html>
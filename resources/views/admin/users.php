<div class="container" ng-controller="HomeController">
	<div class="block-white content small-4 column">
		<h2>Lista de usuarios</h2>
		<ul>
			<li ng-repeat="u in users">
				<a class="right"><i class="icon-edit"></i></a>
				{{u.name}} <br/>
				{{u.mail}}
			</li>
		</ul>
		<hr/>
		<a class="button"><i class="icon-user-plus"></i> Crear nuevo usuario</a>
	</div>

	<div class="block-white content small-4 column">
		<h3>Crear nuevo usuarios</h3>

		<form action="">
			<label for="name">Nombre: <input id="name" type="text"> </label>
			<label for="last_name">Apellidos: <input id="last_name" type="text"> </label>
			<label for="email">E-mail: <input id="email" type="text"> </label>
		</form>
	</div>
</div>
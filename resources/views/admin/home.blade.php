@extends('ci.tpl_admin')

@section('content')
	<div id="header">
		<div class="right">
			@if (!Auth::guest())
				<a href="{{ url('/admin') }}">Administrador</a> | <a href="{{ url('/auth/logout') }}">Cerrar sessión</a>
			@else
				<a href="{{ url('/auth/login') }}">Iniciar sessión</a>
			@endif
		</div>
		<div class="header-logo">
			<object width="100%" type="image/svg+xml" data="{{ asset('svg_sprites/logo.svg') }}">
				<img src="{{ asset("svg_sprites/logo.png") }}" alt="">
			</object>
		</div>
	</div>

	<!-- End header -->

	<div id="main-menu">
		<ul class="block-white">
			<li><a href="#/"><i class="main-menu-icon icon-apps"></i><span class="main-menu-label">Dashboard</span></a></li>
			<li><a href="#users"><i class="main-menu-icon icon-users"></i><span class="main-menu-label">Usuarios</span></a></li>
			<li><a href="#security"><i class="main-menu-icon icon-watch2"></i><span class="main-menu-label">Seguridad</span></a></li>
			<li><a href="#config"><i class="main-menu-icon icon-cog"></i><span class="main-menu-label">Config</span></a></li>
		</ul>
	</div>

	<div id="content">
		<div ng-view></div>
	</div>
@endsection
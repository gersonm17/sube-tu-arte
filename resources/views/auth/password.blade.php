@extends('ci.tpl_site')

@section('content')
	<div class="column small-centered large-4 medium-5">
		<div class="small-centered column logo-small">
			<object type="image/svg+xml" data="{{ asset('svg_sprites/logo.svg') }}">
				<img src="{{ asset('svg_sprites/logo.png') }}" alt="">
			</object>
		</div>
		<h3>Restrablecer contraseña</h3>

		@if (session('status'))
			<div class="alert-box">
				   <strong>Información!</strong>
				{{ session('status') }}
			</div>
		@endif

		@if (count($errors) > 0)
			<div class="alert-box">
				<strong>Información!</strong>
				@foreach ($errors->all() as $error)
					<li style="color: #ffffff">{{ $error }}</li>
				@endforeach
			</div>
		@endif

		<form role="form" method="POST" action="{{ url('/password/email') }}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<label>E-Mail</label>
			<input type="email" name="email" value="{{ old('email') }}">
			<div class="text-center">
				<button type="submit" class="large">Continuar</button>
				<button type="submit" class="">Continuar</button>
			</div>
			<a style="font-size: 14px" href={{url('/auth/login')}}>Volver al Login</a>
		</form>
    </div>
@endsection
@extends('ci.tpl_site')

@section('content')
	<div class="panel">
		<div class="text-center"><h4>Restablecer contraseña</h4></div>
		@if (count($errors) > 0)
			<div class="alert-box" >
				<strong>Información!</strong>
				@foreach ($errors->all() as $error)
					<li style="color: #ffffff">{{ $error }}</li>
				@endforeach
			</div>
		@endif

		<form role="form" method="POST" action="{{ url('/password/reset') }}">
			<input type="hidden" name="token" value="{{$token}}">
			<label>E-Mail</label>
			<input type="email" name="email" value="{{old('email')}}">

			<label>Nueva Contraseña</label>
			<input type="password" name="password">

			<label>Repetir Contraseña</label>
			<input type="password" name="password_confirmation">

			<div class="text-center">
				<button type="submit" class="button tiny">
					Continuar
				</button>
			</div>
		</form>
	</div>
@endsection


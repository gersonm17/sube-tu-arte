@extends('ci.tpl_site')

@section('content')
	<div class="column small-centered large-4 medium-5">
		<form role="form" method="POST" action="{{ url('/auth/login') }}">
			<div class="small-centered column logo-small">
				<object type="image/svg+xml" data="{{ asset('svg_sprites/logo.svg') }}">
					<img src="{{ asset('svg_sprites/logo.png') }}" alt="">
				</object>
			</div>

			@if (count($errors) > 0)
				<div class="alert-box" >
					<strong>Informacion!</strong>
					@foreach ($errors->all() as $error)
						<li style="color: #ffffff">{{ $error }}</li>
					@endforeach
				</div>
			@endif

			<h3>Iniciar sesión</h3>
			<label>E-Mail:</label>
			<input type="email" name="email" placeholder="Ingresa tu correo electrónico" value="{{ old('email') }}">

			<label>Password:</label>
			<input type="password" name="password" placeholder="Ingresa tu contraseña">

			<p class="switch round tiny">
				<input id="member" type="checkbox" name="remember">
				<label for="member">a</label>
				Mantener mi sesión activa
			</p>

			<div class="text-center">
				<button type="submit">Ingresar</button>
				<p><a href="{{ url('/password/email') }}">¿Olvidaste tu usuario o contraseña?</a></p>
			</div>
		</form>
	</div>
@endsection
@extends('layout.template')

@section('content')
    <div class="single_page">
        <header class="single_page__title">
            <h1>Sobre nosotros</h1>
        </header>
        <article class="single_page__entry">
            <p>Lorem ipsum dolor sit amet, noster eruditi iudicabit mea te, has an legere delenit. Vis no solum liberavisse, et elitr mediocritatem mea. Eum regione quaeque eligendi ut, ut ius delenit lobortis. Duo ut aliquam philosophia, eu quot copiosae ius.</p>

            <p>Ea qui nibh gloriatur. Per brute movet ex. Nobis signiferumque ex est. No veniam fabulas adolescens ius, nam te inimicus tincidunt, at modo probatus vim.</p>

            <p>At quas explicari neglegentur qui. Vim decore mollis cu. Omnis expetendis te mei, qui ei postulant urbanitas, eu dicta democritum pro. Per ad tantas temporibus, eos ea utinam quidam elaboraret. Vim eu habeo malis, in purto error sea.</p>

            <p>Usu te adhuc idque facer, et dolorem epicurei fabellas pro. In ubique iracundia sadipscing mel. Dicam veniam at nam, eam everti scripta et, no eos zril fabellas aliquando. Minim partem tractatos ea sit, no habemus accusamus vel. Te vocibus rationibus nec, quidam reprehendunt vix et, et pro quidam forensibus. Partem fastidii convenire vix cu, ad eam labore invidunt.</p>

            <p>Ea has quot suavitate, duo ex maiestatis interesset quaerendum. Pri in dicant option, bonorum disputationi no his. Error placerat eam cu. No quis noster ponderum usu. Eu quot apeirian vix, eu vel fugit constituam interpretaris.</p>
        </article>
    </div>
@endsection
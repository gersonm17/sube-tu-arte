<div class="modal">
	<div class="modal_container">
		<header class="modal_container_header">
			<section class="logo">
				<a href="{{ url('/') }}" title="Sube tu arte">
					<img src="{{ asset('assets/web/images/sube-tu-arte.png') }}" alt="Sube tu arte">
				</a>
            </section>
		</header>

		<section class="modal_container_body">
			<p>Si tienes un alma artista, este es el lugar para ti!</p>

			<p>Muestra tu Arte es una vitrina en la que puedes difundir tu arte o gustar del trabajo de varios artistas totalmente gratis. Disfruta de cantantes, bailarines, bandas, instrumentistas, compositores, todo tipo de diseñadores, artistas plásticos, escultores, fotógrafos, actores, pintores, poetas, escritores y más, desde cualquier parte del mundo!</p>
		</section>

		<footer class="modal_container_footer">
			<button type="button">Ver Artes</button>
		</footer>
	</div>
</div>
<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/


	"password" => "La contraseña deben tener al menos 6 caracteres.",
	"user" => "No podemos encontrar un usuario con ese e-mail.",
	"token" => "Esta URL de restablecimiento de contraseña ya no es válida.",
	"sent" => "La información sobre el reinicio de su contraseña le ha sido enviada por e-mail!",
	"reset" => "Su Contraseña ha sido restablecida con éxito!",

];
